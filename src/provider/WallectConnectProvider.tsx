"use client";
 
import { ThirdwebProvider } from "@thirdweb-dev/react";
import {
    metamaskWallet,
    coinbaseWallet,
    walletConnect,
    okxWallet,
    
  } from "@thirdweb-dev/react";

import { embeddedWallet } from "@thirdweb-dev/react";

export const WalletConnectionProvider =({children}:any) =>  {
 
    return (
        <ThirdwebProvider 
         supportedWallets={[
          metamaskWallet({
            recommended: true,
          }),
          coinbaseWallet(),
          walletConnect(),
          okxWallet(),
          embeddedWallet({
            auth: {
              options: ["email", "google", "facebook", "apple"],
            },
            recommended: true,
          })
        ]}

        activeChain="ethereum"
      clientId="b2f96c510451a497c74adae7083e5245">
        {children}
      </ThirdwebProvider>
    )
      
  }