import { connect } from "@/dbConnection/dbConnection";
import User from "@/models/userModels";
import {NextRequest, NextResponse} from 'next/server'


connect()


export async function POST(request:NextRequest){
    try {
        const reqBody= await request.json();
        const {token} = reqBody;

        const user = await User.findOne({ verifytoken: token , verifyTokenExpiry: {$gt:Date.now()}});

        if(!user){
            return NextResponse.json({error:"invalid token"},{status:400})
        }
        user.isVerified= true;
        user.verifytoken = undefined;
        user.verifyTokenExpiry = undefined;
        await user.save()

        return NextResponse.json({message:"email verified", success:true})

    } catch (error:any) {
        return NextResponse.json({error:error.message},{status:500})
    }
}