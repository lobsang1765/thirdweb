import { connect } from "@/dbConnection/dbConnection";
import User from "@/models/userModels";
import { NextRequest, NextResponse } from 'next/server';
import bcryptjs from 'bcryptjs';
import { sendEmail } from "@/utils/mailer";
connect()

export async function POST(request: NextRequest){
    try {
      const reqBody =   request.json();
      const {username, email, password} = await reqBody;

    //   validation
        const user = await User.findOne({email});
        if(user){
            return NextResponse.json(
                {
                    error:"User already existes"
                },{
                    status:400
                }
            )
        }
        const hashedPassword = await bcryptjs.hash(password, 10)
        let newUser = await new User({
            username,
            email,
            password:hashedPassword
        })

        const savedUser = await newUser.save();

        await sendEmail({email, emailType:"VERIFY", userId:savedUser._id});

        return NextResponse.json(
            { message:"User registered successfully",
            success:true,
            savedUser
        }
        )


    } catch (error:any) {
        return NextResponse.json(
            { error:error.message }, 
            { status:500 }
    )
    }
}