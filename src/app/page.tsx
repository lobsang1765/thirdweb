"use client"
import { ConnectWallet, useAccounts,embeddedWallet } from "@thirdweb-dev/react";
import { useAddress, useDisconnect  } from "@thirdweb-dev/react";
import { Button } from "@/components/ui/button";
export default function Home() {

  const address = useAddress();
  const disconnect = useDisconnect();


  return (
    <>
    {
      address ? (<Button onClick={disconnect}>logout</Button>) :(<ConnectWallet />)
    }
    
    </>
    
  );
}
