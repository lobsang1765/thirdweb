import { log } from "console";
import mongoose from "mongoose";

export async function connect(){
    try {
        mongoose.connect(process.env.MONGO_URL!)
        const connection = mongoose.connection;
        connection.on("connected", ()=>{
            console.log("db connected")
        })

        connection.on('error', ()=>{
            console.log("connection error");
            process.exit()
        })
        
    } catch (error) {
        log("smth went wrong", error)
    }
}