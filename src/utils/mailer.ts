import nodemailer from 'nodemailer';
import bcryptjs from 'bcryptjs'
import User from '@/models/userModels';
export const sendEmail = async ({email, emailType, userId}:any) => {
    try {
        let p;
        const hashtoken = await bcryptjs.hash(userId.toString(), 10);
        if(emailType === "VERIFY"){
            await User.findByIdAndUpdate(userId,{
                verifyToken:hashtoken,
                verifyTokenExpirey: Date.now() + 3600000
            })
            p =  `
                <p>
                Click <a href="${process.env.DOMAIN}/verifyemail?token=${hashtoken}">here</a> to ${emailType === "VERIFY"?"Verify your email":"reset your paswword"}
                or copy and paste teh link below in browers
                <br>${process.env.DOMAIN}/verifyemail?token=${hashtoken}
                </p>
            `
        }else if(emailType === "RESET"){
            await User.findByIdAndUpdate(userId,{
                forgetPasswordToken:hashtoken,
                forgetPasswordTokenExpirey: Date.now() + 3600000
            })
            p =  `
            <p>
            Click <a href="${process.env.DOMAIN}/resetpassword?token=${hashtoken}">here</a> to ${emailType === "VERIFY"?"Verify your email":"reset your paswword"}
            or copy and paste teh link below in browers
            <br>${process.env.DOMAIN}/resetpassword?token=${hashtoken}
            </p>
        `
        }
        var transport = nodemailer.createTransport({
            host: "sandbox.smtp.mailtrap.io",
            port: 2525,
            auth: {
              user: "87136f30f877be",
              pass: "********7b8a"
            }
          });

        const mailOptions = {
            from: 'llll',
            to: email,
            subject: emailType === "VERIFY" ?"Verify your email":"REset your password", 
            html:p, 
        }

        const mailResponse = await transport.sendMail(mailOptions)
    } catch (error:any) {
        throw new Error(error.message)
    }
}
